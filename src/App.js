import { useState } from "react";
import { initialItems } from "./initialItems";
import { v4 as uuidv4 } from "uuid";

export default function App() {
  const [items, setItems] = useState(initialItems);

  function handleDeleteItem(id) {
    setItems(items.filter((item) => item.id !== id));
  }

  function handleToggleItem(id) {
    setItems(
      items.map((item) =>
        item.id === id ? { ...item, done: !item.done } : item
      )
    );
  }

  return (
    <div className="container mt-5">
      <div className="row justify-content-center">
        <div className="col-8">
          <ShoppingList
            items={items}
            onDeleteItem={handleDeleteItem}
            onToggleItem={handleToggleItem}
          />
          <Total items={items} />
          <Form
            setItems={(newItem) => setItems((items) => [...items, newItem])}
          />
        </div>
      </div>
    </div>
  );
}

function ShoppingList({ items, onDeleteItem, onToggleItem }) {
  return (
    <div className="card">
      <div className="card-body">
        <h3 className="card-title">Bevásárló lista</h3>
        <ul className="list-group list-group-flush">
          {items.map((item) => (
            <ListItem
              item={item}
              key={item.id}
              onDeleteItem={onDeleteItem}
              onToggleItem={onToggleItem}
            />
          ))}
        </ul>
      </div>
    </div>
  );
}

function ListItem({ item, onDeleteItem, onToggleItem }) {
  return (
    <li className="list-group-item d-flex justify-content-between align-items-center">
      <div className="form-check">
        <input
          type="checkbox"
          className="form-check-input"
          checked={item.done ? true : false}
          onChange={() => onToggleItem(item.id)}
        />
        <label className={item.done ? "text-decoration-line-through" : ""}>
          {item.name}
        </label>
        <span className="badge text-bg-primary ms-2">{item.quantity}</span>
      </div>
      <button
        type="button"
        className="btn btn-outline-danger btn-sm"
        onClick={() => onDeleteItem(item.id)}
      >
        <i className="bi bi-trash"></i>
      </button>
    </li>
  );
}

function Total({ items }) {
  const toBuy = items.filter((item) => item.done === false);

  return (
    <div className="card mt-3 bg-primary bg-opacity-10">
      <div className="card-body">
        <div className="d-flex justify-content-between">
          <span>Összesen:</span>
          <span>{items.length} db</span>
        </div>
        <div className="d-flex justify-content-between">
          <span>Hátralevő elemek:</span>
          <span>{toBuy.length} db</span>
        </div>
      </div>
    </div>
  );
}
function Form({ setItems }) {
  const [name, setName] = useState("");
  const [quantity, setQuantity] = useState(1);

  function handleSubmit() {
    if (!name) return;

    const newItem = {
      id: uuidv4(),
      name,
      quantity,
      done: false,
    };
    setItems(newItem);

    setName("");
    setQuantity(1);
  }

  return (
    <div className="mt-5">
      <div className="input-group">
        <input
          type="text"
          className="form-control form-control-lg"
          placeholder="Új listaelem..."
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <span className="input-group-text">
          <select
            className="form-select"
            value={quantity}
            onChange={(e) => setQuantity(e.target.value)}
          >
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </span>
      </div>
      <div className="d-grid mt-3">
        <button className="btn btn-primary btn-lg" onClick={handleSubmit}>
          Hozzáadás
        </button>
      </div>
    </div>
  );
}
